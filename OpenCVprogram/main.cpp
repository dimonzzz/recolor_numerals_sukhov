#include <iostream>
#include <opencv2/opencv.hpp>
#include <vector>
#include <stdio.h>
#include <stdlib.h>

using namespace cv;
using namespace std;

Mat m, m_gray;
RNG rng(35548);
int thresh = 100;
int threshold_value = 0;
int threshold_type = 3;;
int const max_value = 255;
int const max_type = 4;
int const max_BINARY_value = 255;

int main()
{
	m = imread("D:/����/����������������/C++/OpenCV/recolor_numerals_sukhov/MyPic.png");
	cout << "Recolor numerals via OpenCV"<<endl;

	imshow("original", m);

	cvtColor(m, m_gray, CV_BGR2GRAY);
	//imshow("Gray", m_gray);
	m_gray = ~m_gray;
	//imwrite("D:/����/����������������/C++/OpenCV/recolor_numerals_sukhov/Gray.png", m_gray);

	Mat output;
	vector<vector<Point> > contours;
	vector<Vec4i> hierarchy;

	//Canny(m_gray, output, thresh, thresh * 2, 3);
	threshold(m_gray, output, threshold_value, max_BINARY_value, threshold_type);
	findContours(output, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));

	Mat drawandfill = Mat::zeros(output.size(), CV_8UC3);
	for (int i = 0; i< contours.size(); i = hierarchy[i][0])
	{
		Scalar color = Scalar(0, 255, 255);
		drawContours(drawandfill, contours, i, color, CV_FILLED, 8, hierarchy);
	}
	imshow("Recolor1", drawandfill);
	drawandfill = ~drawandfill;
	imshow("Recolor2", drawandfill);
	imwrite("D:/����/����������������/C++/OpenCV/recolor_numerals_sukhov/Recolor.png", drawandfill);

	waitKey(0);
	return 0;
}

